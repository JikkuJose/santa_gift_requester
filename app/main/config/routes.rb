# See https://github.com/voltrb/volt#routes for more info on routes

client '/about', action: 'about'
client '/new_gift', action: 'new_gift'
client '/gifts', action: 'gifts'

get '/api/', controller: 'reporter', action: 'show'
post '/api/report', controller: 'reporter', action: 'create'

client '/', {}
