module Main
  class MainController < Volt::ModelController
    LATTITUDE_RANGE = 8.3..8.76
    LONGITUDE_RANGE = 76.8..77.2
    model :store

    def index
      setup_map
      page._markers << 'Kowdiar, Trivandrum'
      page._markers << 'Sashtamangalam, Trivandrum'
    end

    def setup_map
      page._zoom = 12
      page._address = 'kowdiar, trivandrum'
    end

    def test_click
      page._zoom = 14
      page._address = 'kuddapanakunnu, trivandrum'
      page._markers << "kudappanakunnu, Trivandrum"
    end

    def all
      setup_map
    end

    def new_gift
    end

    def request_gift
      store._gifts.create(data(page._gift, page._name))

      flash._successes << "We have requested your gift!"
      page._name = ''
      page._gift = ''
    end

    def last_message
      -> { _messages.last.then { |m| m._message } }.watch!
    end

    def gifts
    end

    private

    def data(gift, requested_by)
      {
        name: gift,
        requested_by: requested_by,
        requested_at: Time.now,
        longitude: rand(LATTITUDE_RANGE),
        lattitude: rand(LONGITUDE_RANGE),
      }
    end

    def main_path
      "#{component}/#{controller}/#{action}"
    end

    def component
      params._component || 'main'
    end

    def controller
      params._controller || 'main'
    end

    def action
      params._action || 'index'
    end

    def active_tab?
      url.path.split('/')[1] == attrs.href.split('/')[1]
    end
  end
end
