module Main
  LATTITUDE_RANGE = 8.3..8.76
  LONGITUDE_RANGE = 76.8..77.2

  class ReporterController < Volt::HttpController
    def show
      render json: {name: 'Jikku Jose'}
    end

    def create
      raw = request.body.read
      h = JSON.parse(raw)
      post_data = data(h["gift"], h["requested_by"])
      store._gifts.create(post_data)

      render text: raw
    end

    def data(gift, requested_by)
        {
          name: gift,
          requested_by: requested_by,
          requested_at: Time.now,
          longitude: rand(LATTITUDE_RANGE),
          lattitude: rand(LONGITUDE_RANGE),
        }
    end
  end
end
