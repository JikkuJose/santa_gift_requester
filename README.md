# Santa Gift Requester

## Project to understand:

- [x] Volt mongo db backend
- [x] Build a HTTP API
- [x] Extract data from query parameters/post data
- [x] Reactively update table based on API post
- [x] Make Google Maps plugin work
- [ ] Make a simple mobile web app to post data
- [ ] Company url in public config
- [ ] Customize look of Google maps
- [ ] Custom pins in Google Maps
- [ ] Design custom pins
- [ ] Bootstrap theming
- [ ] Setup server

## Instructions

1. Clone repo
2. `bundle install`
3. Start mongo dameon: `mongod`
4. Start Volt Server: `bundle exec volt server`
5. Vist: http://localhost:3000

## Development

* Please consider working on separate branch
